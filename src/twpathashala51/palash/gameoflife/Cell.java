package twpathashala51.palash.gameoflife;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Models the smallest entity of a gameboard.
class Cell {
  private final int xCoordinate;
  private final int yCoordinate;
  private final Set<Cell> neighbours;

  Cell(int xCoordinate, int yCoordinate) {
    this.xCoordinate = xCoordinate;
    this.yCoordinate = yCoordinate;
    this.neighbours = new HashSet<>();
  }

  protected void generateNeighbours(){
    for(int x=xCoordinate-1; x<=xCoordinate+1; x++){
      for(int y=yCoordinate-1; y<=yCoordinate+1; y++){
        if(x==xCoordinate && y==yCoordinate)
          continue;
        else
          neighbours.add(new Cell(x,y));
      }
    }
  }

  int numberOfAliveNeighbours(List<Cell> aliveCells){
    int numberOfAliveNeighbours = 0;
    for(Cell neighbour: neighbours){
      if(aliveCells.contains(neighbour))
        numberOfAliveNeighbours++;
    }
    return numberOfAliveNeighbours;
  }


  Set<Cell> getDeadNeighbours(List<Cell> aliveCells){
    Set<Cell> deadNeighbours = new HashSet<>();
    for(Cell neighbour: neighbours){
      if(! aliveCells.contains(neighbour))
        deadNeighbours.add(neighbour);
    }
    return deadNeighbours;
  }

  boolean canComeBackToLife(List<Cell> aliveCells) {
    int numberOfAliveNeighbours = numberOfAliveNeighbours(aliveCells);
    return numberOfAliveNeighbours == 3;
  }

  boolean staysForNextGeneration(List<Cell> aliveCells) {
    int numberOfAliveNeighbours = numberOfAliveNeighbours(aliveCells);
    return numberOfAliveNeighbours == 2 || numberOfAliveNeighbours == 3;
  }


  @Override
  public int hashCode() {
    return 10 * this.xCoordinate + 20 * this.yCoordinate;
  }

  @Override
  public boolean equals(Object anotherCell) {
    if (anotherCell == null)
      return false;
    if (anotherCell.getClass() != Cell.class)
      return false;
    return this.xCoordinate == ((Cell) anotherCell).xCoordinate && this.yCoordinate == ((Cell) anotherCell).yCoordinate;
  }

  @Override
  public String toString() {
    return xCoordinate + "," + yCoordinate;
  }
}
