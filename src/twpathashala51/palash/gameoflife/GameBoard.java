package twpathashala51.palash.gameoflife;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// Models the state of the gameboard
public class GameBoard {
  private final List<Cell> aliveCells;
  private final Set<Cell> deadCells;

  GameBoard(List<Cell> seedCoordinates) {
    this.aliveCells = seedCoordinates;
    deadCells = generateDeadNeighboursOfLiveCells();
    generateDeadNeighboursOfLiveCells();
  }

  private void checkLiveNeighbours(List<Cell> nextGeneration) {
    for (Cell currentCell : aliveCells) {
      if (currentCell.staysForNextGeneration(aliveCells))
        nextGeneration.add(currentCell);
    }
  }

  private void checkDeadNeighbours(List<Cell> nextGeneration){
    for (Cell currentCell : deadCells) {
      currentCell.generateNeighbours();
      if (currentCell.canComeBackToLife(aliveCells))
        nextGeneration.add(currentCell);
    }
  }

  private Set<Cell> generateDeadNeighboursOfLiveCells(){
    Set<Cell> deadCells = new HashSet<Cell>();
    Set<Cell> deadNeighbours;
    for (Cell currentCell : aliveCells){
      currentCell.generateNeighbours();
      deadNeighbours = currentCell.getDeadNeighbours(aliveCells);
      deadCells.addAll(deadNeighbours);
    }
    return deadCells;
  }

  List<Cell> playNextRound() {
    List<Cell> nextGeneration = new ArrayList<>();
    checkLiveNeighbours(nextGeneration);
    checkDeadNeighbours(nextGeneration);
    return nextGeneration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    GameBoard gameBoard = (GameBoard) o;

    if (aliveCells != null ? !aliveCells.equals(gameBoard.aliveCells) : gameBoard.aliveCells != null) return false;
    return deadCells != null ? deadCells.equals(gameBoard.deadCells) : gameBoard.deadCells == null;

  }

  @Override
  public int hashCode() {
    int result = aliveCells != null ? aliveCells.hashCode() : 0;
    result = 31 * result + (deadCells != null ? deadCells.hashCode() : 0);
    return result;
  }
}
