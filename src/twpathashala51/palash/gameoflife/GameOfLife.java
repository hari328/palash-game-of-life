package twpathashala51.palash.gameoflife;

import java.util.ArrayList;
import java.util.List;

//Manages the gameboard
public class GameOfLife {
  private GameBoard gameBoard;

  GameOfLife(List<Cell> seedCoordinates) {
    gameBoard = new GameBoard(seedCoordinates);
  }

  public List<Cell> playNextRound() {
    return gameBoard.playNextRound();
  }
}
