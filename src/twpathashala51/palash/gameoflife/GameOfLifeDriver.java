package twpathashala51.palash.gameoflife;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class GameOfLifeDriver {

  private static final String INPUT_FILENAME = "res/twpathashala51/palash/gameoflife/input.txt";
  private static final String OUTPUT_FILENAME = "res/twpathashala51/palash/gameoflife/output.txt";

  private List<Cell> readInput(Reader readerInterface) {
    List<Cell> seedCoordinates = new ArrayList<>();
    try {
      BufferedReader inputBuffer = new BufferedReader(readerInterface);
      String line = null;
      while (((line = inputBuffer.readLine()) != null) && (!line.equals(""))) {
        seedCoordinates.add(parseLine(line));
      }
      inputBuffer.close();
    } catch (FileNotFoundException e) {
      System.out.println("The file was not found. Exiting");
      System.exit(1);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return seedCoordinates;
  }

  private Cell parseLine(String line) {
    int xCoordinate = Integer.parseInt(line.split(",")[0]);
    int yCoordinate = Integer.parseInt(line.split(",")[1].replace(" ", ""));
    return new Cell(xCoordinate, yCoordinate);
  }

  private void printOutput(Writer outputInterface, List<Cell> nextGeneration) {
    try {
      BufferedWriter outputWriter = new BufferedWriter(outputInterface);
      for (Cell cell : nextGeneration) {
        outputWriter.write(cell.toString());
        outputWriter.newLine();
      }
      outputWriter.close();
    } catch (IOException e) {
      System.out.println("There was an error in trying to print the output.");
    }
  }

  public static void main(String args[]) {
    GameOfLifeDriver gameDriver = new GameOfLifeDriver();
    Reader readerInterface = new InputStreamReader(System.in);
    List<Cell> seedCoordinates = gameDriver.readInput(readerInterface);
    GameOfLife newGame = new GameOfLife(seedCoordinates);
    List<Cell> nextGeneration = newGame.playNextRound();
    Writer outputInterface = new OutputStreamWriter(System.out);
    gameDriver.printOutput(outputInterface, nextGeneration);
  }
}
