package twpathashala51.palash.gameoflife;

import org.junit.Assert;
import org.junit.Test;

public class CellTest {
  @Test
  public void twoCellsWithSameCoordinatesShouldBeEqual() {
    Cell firstCell = new Cell(0, 0);
    Cell secondCell = new Cell(0, 0);
    Assert.assertEquals(firstCell, secondCell);
  }

  @Test
  public void twoCellsWithDifferentCoordinatesShouldNotBeEqual() {
    Cell firstCell = new Cell(0, 0);
    Cell secondCell = new Cell(1, 0);
    Assert.assertNotEquals(firstCell, secondCell);
  }
}
